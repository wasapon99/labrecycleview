package com.example.bakery.recyclerviewdemo.item;


import static com.example.bakery.recyclerviewdemo.adepter.DataAdepter.CATEGORY_ITEM;

public class CategoryItem implements Item<String> {
    private String item;

    public CategoryItem(String item) {
        this.item = item;
    }

    @Override
    public int getType() {
        return CATEGORY_ITEM;
    }

    @Override
    public String getItem() {
        return item;
    }

    @Override
    public void setItem(String item) {
        this.item = item;
    }
}
