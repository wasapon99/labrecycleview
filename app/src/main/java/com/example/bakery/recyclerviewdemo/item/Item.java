package com.example.bakery.recyclerviewdemo.item;

/**
 * Created by Bakery on 1/23/2018.
 */

public interface Item<I> {
    int getType();

    I getItem();

    void setItem(I item);
}
