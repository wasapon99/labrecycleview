package com.example.bakery.recyclerviewdemo.adepter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.bakery.recyclerviewdemo.R;

/**
 * Created by Bakery on 1/23/2018.
 */

public class CategoryViewHolder extends RecyclerView.ViewHolder {
    private TextView tvBigTitle;

    public CategoryViewHolder(View itemView) {
        super(itemView);
        tvBigTitle = itemView.findViewById(R.id.tvBigTitle);
    }

    public void setCategory(String item) {
        tvBigTitle.setText(item);
    }
}
