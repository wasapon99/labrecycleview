package com.example.bakery.recyclerviewdemo;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.bakery.recyclerviewdemo.adepter.DataAdepter;
import com.example.bakery.recyclerviewdemo.item.CategoryItem;
import com.example.bakery.recyclerviewdemo.item.Item;
import com.example.bakery.recyclerviewdemo.item.ProductItem;
import com.example.bakery.recyclerviewdemo.model.Book;
import com.example.bakery.recyclerviewdemo.model.Category;

import java.util.ArrayList;
import java.util.List;

import static com.example.bakery.recyclerviewdemo.adepter.DataAdepter.CATEGORY_ITEM;
import static com.example.bakery.recyclerviewdemo.adepter.DataAdepter.PRODUCT_ITEM;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rvView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false);
        recyclerView.setLayoutManager(layoutManager);
        createAdapter(createItem());

    }

    public void createAdapter(List<Item> itemList) {
        final DataAdepter adapter = new DataAdepter(itemList);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case CATEGORY_ITEM:
                        return 2;
                    case PRODUCT_ITEM:
                        return 1;
                    default:
                        return 1;
                }
            }
        });
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    public List<Item> createItem() {
        List<Item> itemList = new ArrayList<>();
        List<Category> categoryList = MockData.getDetail();

        if (categoryList != null)
            for (Category category : categoryList) {
                itemList.add(new CategoryItem(category.getTitle()));
                for (Book book : category.getbooksList()) {
                    itemList.add(new ProductItem(book));
                }
            }

        return itemList;
    }

}
