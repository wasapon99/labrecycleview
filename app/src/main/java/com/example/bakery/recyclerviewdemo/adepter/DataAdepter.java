package com.example.bakery.recyclerviewdemo.adepter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.bakery.recyclerviewdemo.R;
import com.example.bakery.recyclerviewdemo.item.Item;
import com.example.bakery.recyclerviewdemo.model.Book;
import java.util.List;

public class DataAdepter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Item> itemList;
    public static final int PRODUCT_ITEM = 0;
    public static final int CATEGORY_ITEM = 1;


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == CATEGORY_ITEM) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_view, parent, false);
            return new CategoryViewHolder(view);
        } else if (viewType == PRODUCT_ITEM) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_view, parent, false);
            return new DataViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CategoryViewHolder) {
            CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
            categoryViewHolder.setCategory((String) itemList.get(position).getItem());
        } else if (holder instanceof DataViewHolder) {
            DataViewHolder dataViewHolder = (DataViewHolder) holder;
            Book modelDataBook = (Book) itemList.get(position).getItem();
            dataViewHolder.setName(modelDataBook.getName());
            dataViewHolder.setTvSeason(modelDataBook.getSeason());
            dataViewHolder.setTvPrice(modelDataBook.getPrice());
            dataViewHolder.setIvImage(modelDataBook.getImage());
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public DataAdepter(List<Item> itemList) {
        this.itemList = itemList;

    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position).getType();
    }



}
