package com.example.bakery.recyclerviewdemo.adepter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bakery.recyclerviewdemo.R;

/**
 * Created by Bakery on 1/23/2018.
 */

public class DataViewHolder extends RecyclerView.ViewHolder {
    public TextView tvName;
    public ImageView ivImage;
    public android.widget.TextView tvSeason;
    public TextView tvPrice;

    public DataViewHolder(View itemView) {
        super(itemView);
        tvName = itemView.findViewById(R.id.tvName);
        ivImage = itemView.findViewById(R.id.ivImage);
        tvSeason = itemView.findViewById(R.id.tvSeason);
        tvPrice = itemView.findViewById(R.id.tvPrice);

    }

    public void setName(String name) {
        tvName.setText(name);
    }

    public void setTvSeason(String season) {
        tvSeason.setText(season);
    }

    public void setTvPrice(String price) {
        tvPrice.setText(price);

    }

    public void setIvImage(int image) {
        ivImage.setBackgroundResource(image);
    }


}
