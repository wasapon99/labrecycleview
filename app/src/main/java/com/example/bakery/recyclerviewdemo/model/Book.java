package com.example.bakery.recyclerviewdemo.model;


public class Book {
    private String name;
    private String season;
    private String price;
    private int image;

    public Book(String name, int image, String season, String price) {
        this.name = name;
        this.image = image;
        this.season = season;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

}
