package com.example.bakery.recyclerviewdemo.item;

import com.example.bakery.recyclerviewdemo.model.Book;

import static com.example.bakery.recyclerviewdemo.adepter.DataAdepter.PRODUCT_ITEM;

public class ProductItem implements Item<Book> {
    private Book item;

    public ProductItem(Book item) {
        this.item = item;
    }

    @Override
    public int getType() {
        return PRODUCT_ITEM;
    }

    @Override
    public Book getItem() {
        return item;
    }

    @Override
    public void setItem(Book item) {
        this.item = item;
    }

}
