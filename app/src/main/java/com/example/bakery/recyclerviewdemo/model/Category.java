package com.example.bakery.recyclerviewdemo.model;

import java.util.List;


public class Category {
    private String title;
    private List<Book> booksList;

    public Category(String title, List<Book> booksList) {
        this.title = title;
        this.booksList = booksList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Book> getbooksList() {
        return booksList;
    }

    public void setbooksList(List<Book> BooksList) {
        this.booksList = BooksList;
    }

}
