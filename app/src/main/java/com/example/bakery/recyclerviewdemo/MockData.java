package com.example.bakery.recyclerviewdemo;

import com.example.bakery.recyclerviewdemo.model.Book;
import com.example.bakery.recyclerviewdemo.model.Category;

import java.util.ArrayList;
import java.util.List;


public class MockData {
    private static String[] title = {"OnePiece", "Naruto", "Bleach", "Fairytail", "Reborn"};
    private static String[] name = {"OnePiece", "Naruto", "Bleach", "Fairytail", "Reborn"};
    private static int[] image = {R.drawable.onepiece, R.drawable.naruto, R.drawable.bleach, R.drawable.fairytail, R.drawable.reborn};
    private static String[] season = {"10", "3", "2", "1", "5",};
    private static String[] price = {"450", "150", "250", "100", "200"};

    public static List<Category> getDetail() {
        List<Category> categoryList = new ArrayList<>();
        for (int i = 0; i < title.length; i++) {
            Category category = new Category(title[i], createBook());
            categoryList.add(category);
        }
        return categoryList;
    }

    public static List<Book> createBook() {
        List<Book> bookList = new ArrayList<>();

        for (int i = 0; i < name.length; i++) {
            Book book = new Book(name[i], image[i], season[i], price[i]);
            bookList.add(book);
        }
        return bookList;
    }
}
